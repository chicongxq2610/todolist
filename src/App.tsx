import './App.css';
import Header from './components/Header'
import TodoList from './components/Todo-list'
import Footer from './components/Footer';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getTodoList } from './reducers/redux-thunk';
import { ToastContainer } from 'react-toastify';

const App: React.FC = () =>  {

  const dispatch = useDispatch()

  useEffect (() => {
    dispatch(getTodoList())
  }, [dispatch])

  return (
    <div className="todoapp">
      <Header />
      <TodoList />
      <Footer />
      <ToastContainer />
    </div>
  );
}

export default App;
