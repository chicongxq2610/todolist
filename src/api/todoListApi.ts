import { ITodo } from "../type"
import axiosClient from "./axiosClient"

const todoListApi = {
    getTodoList: () => {
        const url = 'todolist'
        return axiosClient.get(url)
    },

    deleteTodo: (id: number) => axiosClient.delete(`todolist/${id}`) ,

    postTodo: (todo: ITodo) => axiosClient.post(`todolist`, todo),

    putTodo: (todo: ITodo) => axiosClient.put(`todolist/${todo.id}`, todo),
}

export default todoListApi