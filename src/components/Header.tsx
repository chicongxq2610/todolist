import { useState, KeyboardEvent } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addTodoThunk } from '../reducers/redux-thunk'
import { IStore } from '../type'

const Header = () => {
    const [text, setText] = useState("")
    const dispatch = useDispatch()
    const lengthTodoList = useSelector((state: IStore) => state.todoReducer.todoList.length)
    
    const onAddTodo = (e: KeyboardEvent) => {
        if (e.key === 'Enter' && text) {
            const todo = {
                id: lengthTodoList + 1,
                title: text,
                completed: false,
                deadline: 0
            }
            dispatch(addTodoThunk(todo))
            setText("")
        }
    }

    return (
        <header className="header">
            <h1>My todos</h1>
            <input 
                value={text}
                className="new-todo" 
                onChange={e => setText(e.target.value)}
                onKeyPress={e => onAddTodo(e)}
            />
        </header>
    )
}

export default Header