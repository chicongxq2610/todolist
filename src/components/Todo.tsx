import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { deleteTodoEditingId, getTodoEditingId } from '../actions/todoActions'
import { deleteTodoThunk, editTodoThunk } from '../reducers/redux-thunk'
import { IStore } from '../type'

interface Props {
    id: number,
    title: string,
    completed: boolean,
    deadline: number
}

const Todo: React.FC<Props> = (props) => {
    const todoEditingId = useSelector((state: IStore) => state.todoReducer.idTodoEditting)
    const dispatch = useDispatch()
    const {id, title, completed, deadline} = props
    const isEditing = todoEditingId === id
    const [textEdit, setTextEdit] = useState(title)
    const [deadlinePicker, setDeadlinePicker] = useState(false)
    const [nearlyDeadline, setNearlyDeadline] = useState(false)

    useEffect(() => {
        const currentTime = new Date().getTime()
        const interval = setInterval(() => {
            if (deadline !== 0) {
                deadline - currentTime <= 3600000 ? setNearlyDeadline(true) : setNearlyDeadline(false) 
            }
        }, 1000);
        return () => clearInterval(interval)
    })

    const onEditTodo = () => {
        dispatch(editTodoThunk({
            id,
            title: textEdit,
            completed,
            deadline
        }))
        dispatch(deleteTodoEditingId())
    }

    const onMarkTodo = () => {
        dispatch(editTodoThunk({
            id,
            title,
            completed: !completed,
            deadline
        }))
    }

    const customDate = (time: number) => 
        time > 9 ? time : '0' + time 

    const convertToDate = () => {
        const deadlineDate = new Date(deadline)
        const hours = customDate(deadlineDate.getHours())
        const minutes = customDate(deadlineDate.getMinutes())
        const day = customDate(deadlineDate.getDate())
        const month = customDate(deadlineDate.getMonth() + 1)
        return hours + ':' + minutes + ' | ' + day + '-' + month + '-' + deadlineDate.getFullYear()
    }

    const onBlurDeadlinePicker = (e: React.ChangeEvent<HTMLInputElement>) => {
        setDeadlinePicker(false) 
        const deadlineDate = new Date(e.target.value)
        dispatch(editTodoThunk({
            id,
            title,
            completed,
            deadline: deadlineDate.getTime()
        }))
    }

    return (
        <li className={`${completed ? 'completed' : ''} ${isEditing ? 'editing' : ''}`}>
            {
                !isEditing ?
                    <div className="view">
                        <input 
                            type="checkbox" 
                            className="toggle" 
                            checked={completed}
                            onChange={onMarkTodo}
                        />
                        <label onDoubleClick={() => dispatch(getTodoEditingId(id))}>
                            <div>{title}</div>
                            <div className="deadline">
                            <div className={deadlinePicker ? 'display-none' : ''}>
                                <button className={deadline === 0 ? 'display-none' : `deadline-content + ${nearlyDeadline ? ' nearly-deadline' : ''}`}>{convertToDate()}</button>
                                <button className={`deadline-content add-deadline-btn + ${nearlyDeadline ? ' nearly-deadline' : ''}`}  onClick={() => setDeadlinePicker(true)}>{ deadline === 0 ? 'Add deadline' : 'Edit deadline' }</button>
                            </div>
                            <input 
                                className={!deadlinePicker ? 'display-none' : ''} 
                                type="datetime-local" 
                                onBlur={e => onBlurDeadlinePicker(e)}
                            />
                        </div>
                        
                        </label>
                        
                        <button className="destroy" onClick={() => dispatch(deleteTodoThunk(id))}></button>
                    </div>
                :
                    <input 
                        className="edit" 
                        type='text'
                        value={textEdit}
                        onChange={e => setTextEdit(e.target.value)}
                        onBlur={() => onEditTodo()}
                        onKeyPress={ (e) => {
                            if (e.key === 'Enter') onEditTodo()
                        }}
                    />
            }
        </li>
    )
}

export default Todo