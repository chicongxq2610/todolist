import { useSelector } from 'react-redux'
import Todo from './Todo'
import { IStore, Status } from '../type'

const TodoList = () => {

    const statusFilter = useSelector((state: IStore) => state.todoReducer.filterStatus)
    let todoList = useSelector((state: IStore) => state.todoReducer.todoList)

    if (statusFilter === Status.Active) todoList = todoList.filter(todo => !todo.completed)
    else if (statusFilter === Status.Completed) todoList = todoList.filter(todo => todo.completed)

    const showTodoList = todoList.map((todo) => 
        <Todo 
            {...todo}
            key={todo.id}    
        />
    )

    return (
        <section className="main">
            <ul className="todo-list">
                {showTodoList}
            </ul>
        </section>
    )
}

export default TodoList