import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { filterTodoList } from '../actions/todoActions'
import { clearCompltedThunk } from '../reducers/redux-thunk'
import { IStore, ITodo, Status } from '../type'
import "react-toastify/dist/ReactToastify.css";



const Footer: React.FC = () => {
    const todoList = useSelector((state: IStore) => state.todoReducer.todoList)
    const statusFilter = useSelector((state: IStore) => state.todoReducer.filterStatus)
    const numsOfTodo = todoList.length
    const numOfTodoCompleted = todoList.filter((todo: ITodo) => todo.completed === true).length
    const dispatch = useDispatch()

    const onClearCompleted = () => {
        todoList.forEach(todo => {
            if (todo.completed) {
                dispatch(clearCompltedThunk(todo.id))
            }
        })
        toast.success("Clear Successfully") 
    }

    const counterBtns = [
        {
            title: Status.All,
            isActive: statusFilter === Status.All,
            counter: numsOfTodo,
            bgColor: '#0069D9',
            onClick: () => dispatch(filterTodoList(Status.All))
        },

        {
            title: Status.Active,
            isActive: statusFilter === Status.Active,
            counter: numsOfTodo - numOfTodoCompleted,
            bgColor: '#218838',
            onClick: () => dispatch(filterTodoList(Status.Active))
        },

        {
            title: Status.Completed,
            isActive: statusFilter ===  Status.Completed,
            counter: numOfTodoCompleted,
            bgColor: '#5A6268',
            onClick: () => dispatch(filterTodoList( Status.Completed))
        }
    ]

    return (
        <footer className="footer">
            <ul className="filters">
                {
                    counterBtns.map(btn => 
                        <CounterBtn key={btn.title} {...btn} />
                    )
                }
                <li>{numOfTodoCompleted !== 0 && <button className="clear-completed" onClick={onClearCompleted}>Clear Completed</button>}</li>
            </ul>
        </footer>
    )
}

interface Props {
    title: string,
    isActive: boolean,
    counter: number,
    bgColor: string,
    onClick: () => void
}

const CounterBtn: React.FC<Props> = (props) => {
    const { title, isActive, counter, bgColor, onClick } = props
    return (
        <>
            <li>
                <span 
                    style={{ backgroundColor: bgColor }}
                    className={isActive ? 'selected' : ''}  
                    onClick={onClick} 
                >
                    {title} : {counter}
                </span>
            </li>
        </>
    )
}

export default Footer