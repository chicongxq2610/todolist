import { toast } from "react-toastify";
import { addTodo, editTodo, removeTodo, setTodoList } from "../actions/todoActions"
import todoListApi from "../api/todoListApi"
import { ITodo } from "../type";
import "react-toastify/dist/ReactToastify.css";

export const getTodoList = () => async (dispatch: Function) => {
    try {
        const response = await todoListApi.getTodoList();
        dispatch(setTodoList(response.data))
    }
    catch (err) {
        alert(err)
    }
} 

export const addTodoThunk = (todo: ITodo) => async (dispatch: Function) => {
    try {
        const response = await todoListApi.postTodo(todo);
        dispatch(addTodo(response.data))
        toast.success("Add Todo successfully")
    }
    catch (err) {
        alert(err)
    }
}

export const deleteTodoThunk = (id: number) => async (dispatch: Function) => {
    try {
        dispatch(removeTodo(id))
        toast.success("Delete successfully")
        await todoListApi.deleteTodo(id)
    }
    catch (err) {
        alert(err)
        toast.error("Delete Failed")
    }
}

export const editTodoThunk = (todo: ITodo) => async (dispatch: Function) => {
    try {
        dispatch(editTodo(todo))
        await todoListApi.putTodo(todo)
    }

    catch (err) {
        alert(err)
    }
}

export const clearCompltedThunk = (id: number) => async (dispatch: Function) => {
    try {
        dispatch(removeTodo(id))
        await todoListApi.deleteTodo(id)
    }
    catch (err) {
        alert(err)
    }
}