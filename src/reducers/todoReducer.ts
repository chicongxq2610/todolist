import { IAction, ITodoState, Status } from "../type"

const initialState: ITodoState = {
    todoList: [],
    idTodoEditting: -1,
    filterStatus: Status.All
}

const todoReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case 'SET_TODO_LIST':
            return {
                ...state,
                todoList: action.payload
            }

        case 'ADD_TODO':
            return {
                ...state,
                todoList: [...state.todoList, action.todo]
            }
        
        case 'EDIT_TODO':
            return {
                ...state,
                todoList: state.todoList.map(todo => 
                    todo.id === action.todo?.id ? action.todo : todo
                )
            }

            case 'EDIT_DEADLINE_TODO':
                return {
                    ...state,
                    todoList: state.todoList.map(todo => 
                        todo.id === action.id ? {...todo, deadline: action.deadline} : todo
                    )
                }

        case 'REMOVE_TODO': 
            return {
                ...state,
                todoList: state.todoList.filter(todo => todo.id !== action.id)
            }
        
        case 'MARK_TODO':
            return {
                ...state,
                todoList: state.todoList.map(todo => 
                    todo.id === action.id ? {...todo, completed: !todo.completed} : todo
                )
            }

        case "GET_EDITING_ID":
            return {
                ...state,
                idTodoEditting: action.id
            }
    
        case "DELETE_EDITING_ID":
            return {
                ...state,
                idTodoEditting: -1
            }

        case 'FILTER_TODO_LIST': 
            return {
                ...state,
                filterStatus: action.status
            }

        default:
            return state
    }
}

export default todoReducer