import { ITodo } from "../type"

export const setTodoList = (todos: ITodo[]) => {
    return {
        type:'SET_TODO_LIST',
        payload: todos
    }
}

export const filterTodoList = (status: string) => {
    return {
        type: 'FILTER_TODO_LIST',
        status
    }
}

export const addTodo = (todo: ITodo) => {
    return {
        type: 'ADD_TODO',
        todo
    }
}

export const editTodo = (todo: ITodo) => {
    return {
        type: 'EDIT_TODO',
        todo
    }
}

export const removeTodo = (id: number) => {
    return {
        type: "REMOVE_TODO",
        id
    };
}

export const getTodoEditingId = (id: number) => {
    return {
        type: "GET_EDITING_ID",
        id
    }
}

export const deleteTodoEditingId = () => {
    return {
        type: "DELETE_EDITING_ID",
    }
}