
export interface ITodo {
    userId?:number,
    id: number,
    title: string,
    completed: boolean,
    deadline: number
}

export interface IAction {
    type: string,
    payload?: ITodo[],
    status?: string,
    id?: number,
    todo?: ITodo,
    deadline?: number,
    text?: string
}

export interface ITodoState {
    todoList: ITodo[],
    idTodoEditting: number,
    filterStatus: string
}

export interface IStore {
    todoReducer: ITodoState
}

export enum Status {
    All = 'All',
    Active = 'Active',
    Completed = 'Completed'
}
